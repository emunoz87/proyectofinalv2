import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LesseePageRoutingModule } from './lessee-routing.module';

import { LesseePage } from './lessee.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LesseePageRoutingModule
  ],
  declarations: [LesseePage]
})
export class LesseePageModule {}
